#include <winsock2.h>
#include <windows.h>
#include <iostream>
#include <fstream>
#include <string>
#include "hook.h"

using namespace std;

DllClass::DllClass() {
	cout << "Test" << endl;
}


DllClass::~DllClass () {
	cout << "End test" << endl;
}

BYTE hook[6];
BYTE hook2[6];
BYTE jmp[6] = { 0xe9,0x00, 0x00, 0x00, 0x00 ,0xc3 };
DWORD pPrevious;

typedef int ( WINAPI *realConnect )(SOCKET s, const struct sockaddr* name, int namelen );
typedef int (WINAPI* realRecv)(SOCKET socket, const char* buffer, int length, int flags);
typedef int (WINAPI* realSend)(SOCKET socket, const char* buffer, int length, int flags);

realConnect o_connect;
realSend o_send;
realRecv o_recv;

void *DetourFunction(BYTE *src, const BYTE *dst, const int len)
{
	BYTE *jmp = (BYTE*)malloc(len+5);
	DWORD dwback;
	VirtualProtect(src, len, PAGE_READWRITE, &dwback);
	memcpy(jmp, src, len); jmp += len;
	jmp[0] = 0xE9;
	*(DWORD*)(jmp+1) = (DWORD)(src+len - jmp) - 5;
	src[0] = 0xE9;
	*(DWORD*)(src+1) = (DWORD)(dst - src) - 5;
	VirtualProtect(src, len, dwback, &dwback);
	return (jmp-len);
}

int WINAPI my_connect( SOCKET s, const struct sockaddr* name, int namelen)
{
	WORD port = ntohs((*(WORD*)name->sa_data));
/*	sockaddr_in *sockaddr = (sockaddr_in*)name;
	sockaddr->sin_port = htons(16000);
	if ( port != 80 )
	{
	sockaddr->sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
	}*/
	cout << "Connecting to port " << endl;
	return o_connect(s,name,namelen);
}

int WINAPI my_send(SOCKET socket, const char* buffer, int length, int flags) {
	cout << "Sending" << endl;
	return o_send(socket, buffer, length, flags);
}

int WINAPI my_recv(SOCKET socket, const char* buffer, int length, int flags) {
	cout << "Recving" << endl;
	return o_recv(socket, buffer, length, flags);
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) {
	HMODULE hWS32 = LoadLibraryA( "ws2_32.dll" );
	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH:
		cout << "OxyBottin attaching...";
		DetourFunction((BYTE *)GetProcAddress(hWS32,"connect"),(CONST BYTE *)my_connect,5);
		__asm__ __volatile__ (
			"movl $o_connect, %eax\n"
		);
		DetourFunction((BYTE *)GetProcAddress(hWS32,"send"),(CONST BYTE *)my_send,5);
		__asm__ __volatile__ (
			"movl $o_send, %eax\n"
		);
		DetourFunction((BYTE *)GetProcAddress(hWS32,"recv"),(CONST BYTE *)my_recv,5);
		__asm__ __volatile__ (
			"movl $o_recv, %eax\n"
		);
		cout << "done" << std::endl;
		MessageBox(HWND_DESKTOP,"Successfully injected.","OxyBottin",MB_OK);
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

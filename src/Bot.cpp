#include <windows.h>
#include <tlhelp32.h>
#include <shlwapi.h>
#include <conio.h>
#include <stdio.h>
#include <tchar.h>
#include <direct.h>
#include "RSC/Client.hpp"
#include "RSC/model/Mob.hpp"

using namespace RSC;

#define WIN32_LEAN_AND_MEAN
#define CREATE_THREAD_ACCESS (PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ)

BOOL Inject(DWORD pID, const char * dll_name ) {
	HANDLE targetProcess, createdThread;
	//HMODULE hLib;
	char buf[50] = {0};
	LPVOID myRemoteString, LoadLibAddy;

	if( ! pID ) {
		return FALSE;
	}

	targetProcess = OpenProcess( CREATE_THREAD_ACCESS, FALSE, pID );
	if( ! targetProcess ) {
		sprintf_s(buf, "OpenProcess() failed: %d", GetLastError());
		MessageBox(NULL, buf, "Loader", MB_OK);
		printf(buf);
		return false;
	}

	LoadLibAddy = (LPVOID)GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");
	if( ! LoadLibAddy ) {
		printf( "ERROR: Problems with GetProcAddress.  Error code: %d\n", GetLastError() );
		return false;
	}

	// Allocate space in the process for the dll
	myRemoteString = (LPVOID)VirtualAllocEx( targetProcess, NULL, strlen(dll_name), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if( ! myRemoteString ) {
		printf( "ERROR: Problems with VirtualAllocEx.  Error code: %d\n", GetLastError() );
		return false;
	}

	// Write the string name of the dll in the memory allocated
	if( ! WriteProcessMemory( targetProcess, (LPVOID)myRemoteString, dll_name, strlen(dll_name), NULL) ) {
		printf( "ERROR: Problems with WriteProcessMemory.  Error code: %d\n", GetLastError() );
		return false;
	}

	// Load the dll
	createdThread = CreateRemoteThread( targetProcess, NULL, NULL, (LPTHREAD_START_ROUTINE)LoadLibAddy, (LPVOID)myRemoteString, NULL, NULL);
	if( ! createdThread ) {
		printf( "ERROR: Problems with CreateRemoteThread.  Error code: %d\n", GetLastError() );
		return false;
	}

	WaitForSingleObject(createdThread, INFINITE);

	// Free the memory that is not being using anymore.
	if( myRemoteString != NULL ) VirtualFreeEx( targetProcess, myRemoteString, 0, MEM_RELEASE );
	if( createdThread != NULL ) CloseHandle( createdThread );
	if( targetProcess != NULL ) CloseHandle( targetProcess );

	//VirtualFreeEx(hProcess , (LPVOID)Memory , 0, MEM_RELEASE);

	return true;
}

DWORD GetTargetThreadIDFromProcName(const char *ProcName) {
	PROCESSENTRY32 pe;
	HANDLE thSnapShot;
	BOOL retval, ProcFound = false;

	thSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if(thSnapShot == INVALID_HANDLE_VALUE) {
		//MessageBox(NULL, "Error: Unable <strong class="highlight">to</strong> create toolhelp snapshot!", "2MLoader", MB_OK);
		printf("Error: Unable to create toolhelp snapshot!");
		return false;
	}

	pe.dwSize = sizeof(PROCESSENTRY32);

	retval = Process32First(thSnapShot, &pe);
	while(retval) {
		if( !strcmp(pe.szExeFile, ProcName) )
			return pe.th32ProcessID;
		retval = Process32Next(thSnapShot, &pe);
	}

	return 0;
}

int _tmain(int argc, _TCHAR* argv[]) {
	// Retrieve process ID
	DWORD pID = GetTargetThreadIDFromProcName("java.exe");
	if( !pID ) {
		printf( "ERROR: Could not find any process for java.exe.\n");
		_getch();
		return 0;
	}

	// Get the dll's full path name
	char buf[MAX_PATH] = {0};
	// GetFullPathName("..\\SimpleDLL.dll", MAX_PATH, buf, NULL);
	char currentDir[FILENAME_MAX];
	_getcwd(currentDir, sizeof(currentDir));
	sprintf_s(buf, "%s\\hooks.dll", currentDir);

	printf( "Dll path = %s\n", buf );

	// Inject our main dll
	if(!Inject(pID, buf)) {
		printf("Dll not loaded.");
	} else {
		printf("Dll loaded.");
	}

	_getch();
	return 0;
}
